# File efoi.csv has the following headers in the first row:
# Num, Title, Agency, Agency Name, Agency Type, Purpose, Coverage, Req ID
# Status-Scraped, Date Requested, Request Link
# Client Chats, Agency Chats, Final Agency Chat, extended, Status-PCOO
# dm_exception, ro_exception


import csv, re, string
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

# function for cleaning html tags
def clean_html(sentence):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, ' ', sentence)
    return cleantext


stop = stopwords.words('english')  # Get the English Language Stopwords

agency = input('Enter Agency:')
criteria = input('Enter criteria:')
criteria_value = input('Enter criteria value:')
target_column = input('Enter Target Column for Text Analysis:')
count = 0
text = ''
tokens_clean = []
token_hist = {}

# read dataset base on given agency and criteria
with open('efoi.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        if row['Agency'] == agency and row[criteria] == criteria_value:
            count += 1
            text = text + ' ' + clean_html(row[target_column])

#print(text)

print("Total number of records processed:",count)

# clean dataset
# 1. Set all to lowercase and then tokenize
tokens = word_tokenize(text.lower())
#print(tokens)
print("Tokens:",len(tokens))

# 2. Remove punctuations, numbers, and stopwords
for word in tokens:
    if word not in string.punctuation: # if token is not a punctuation then
        if word.isalpha(): # check if it is not a number then
            if word not in stop: # check if it is not a stopword
                tokens_clean.append(word) # then store it

#print(tokens_clean)
print("Cleaned tokens:", len(tokens_clean))

# create histogram
for word in tokens_clean:
    token_hist[word] = token_hist.get(word,0) + 1

# sort the histogram
for num, word in sorted([(value,key) for (key,value) in token_hist.items()],reverse=True):
    print(word,":",num)

