# File efoi.csv has the following headers in the first row:
# Num, Title, Agency, Agency Name, Agency Type, Purpose, Coverage, Req ID
# Status-Scraped, Date Requested, Request Link
# Client Chats, Agency Chats, Final Agency Chat, extended, Status-PCOO
# dm_exception, ro_exception


import csv, re, string, math
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

# function for cleaning html tags
def clean_html(sentence):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, ' ', sentence)
    return cleantext


stop = stopwords.words('english')  # Get the English Language Stopwords

agency = input('Enter Agency:')
criteria = input('Enter criteria:')
criteria_value = input('Enter criteria value:')
target_column = input('Enter Target Column for Text Analysis:')
count = 0
text = []
tokens_clean = []
doc_list = []
tfDict_list = []


# read dataset base on given agency and criteria
with open('efoi.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        if row['Agency'] == agency and row[criteria] == criteria_value:
            count += 1
            text.append(clean_html(row[target_column]))

#print(text)
print("Total number of records processed:",count)

# clean dataset to produce a list of list
for i in text:
    # 1. Set all to lowercase and then tokenize
    tokens = word_tokenize(i.lower())
    # 2. Remove punctuations, numbers, and stopwords
    for word in tokens:
        if word not in string.punctuation: # if token is not a punctuation then
            if word.isalpha(): # check if it is not a number then
                if word not in stop: # check if it is not a stopword
                    tokens_clean.append(word) # then store it to temp list
    # store the list to the doc_list
    doc_list.append(tokens_clean)
    tokens_clean = [] # reset tokens_clean

#print(doc_list)
print("Doc List Count:", len(doc_list))

# Function for computing a TF Map
def computeTF(document):
    """ Returns a tf dictionary for each document whose keys are all
    the unique words in the document and whose values are their
    corresponding tf.
    """
    #Counts the number of times the word appears in document
    tfDict = {}
    for w in document:
        if w in tfDict:
            tfDict[w] += 1
        else:
            tfDict[w] = 1
    #Computes tf for each word
    for w in tfDict:
        tfDict[w] = tfDict[w] / len(document)
    return tfDict

# Compute TF Map and Store in a list
for doc in doc_list:
    print(computeTF(doc))

# Function for computing an IDF Map
